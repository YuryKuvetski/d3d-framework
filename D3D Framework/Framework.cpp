#include "stdafx.h"
#include "Framework.h"
#include "macros.h"
#include "Log.h"

namespace D3D11Framework
{
//------------------------------------------------------------------

	Framework::Framework() :
		_pWnd(nullptr),
		_pRender(nullptr),
		_pInput(nullptr),
		_init(false)
	{
	}

	Framework::~Framework()
	{
	}

	void Framework::AddInputListener(InputListener *listener)
	{
		if (_pInput)
			_pInput->AddListener(listener);
	}

	void Framework::Run()
	{
		if (_init)
			while(_frame());
	}

	bool Framework::Init(const FrameworkDesc &descFramework)
	{
		_pRender = descFramework._pRender;

		_pWnd = new Window();
		_pInput = new InputMgr();

		if (!_pWnd || !_pInput)
		{
			Log::Get()->Err("�� ������� �������� ������");
			return false;
		}

		_pInput->Init();

		// ������� �������� �������� �� ���������. � ����� �� ������� ������ �� �������� � �����
		if (!_pWnd->Create(descFramework._wnd))
		{
			Log::Get()->Err("�� ������� ������� ����");
			return false;
		}
		_pWnd->SetInputMgr(_pInput);

		if (!_pRender->CreateDevice(_pWnd->GetHWND()))
		{
			Log::Get()->Err("�� ������� ������� ������");
			return false;
		}

		_init = true;
		return true;
	}

	bool Framework::_frame()
	{
		// ������������ ������� ����
		_pWnd->RunEvent();
		// ���� ���� ��������� - ��������� ����
		if (!_pWnd->IsActive())
			return true;

		// ���� ���� ���� �������, ��������� ������ ������
		if (_pWnd->IsExit())
			return false;

		// ���� ���� �������� ������
		if (_pWnd->IsResize())
		{
		}

		_pRender->BeginFrame();
		if (!_pRender->Draw())
			return false;
		_pRender->EndFrame();

		return true;
	}

	void Framework::Close()
	{
		_init = false; 
		_pRender->Shutdown();
		_DELETE(_pRender);
		_CLOSE(_pWnd);
		_CLOSE(_pInput);
	}

//------------------------------------------------------------------
}