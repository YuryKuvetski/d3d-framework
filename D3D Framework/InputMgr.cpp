#include "stdafx.h"
#include "InputMgr.h"
#include "InputCodes.h"
#include "InputListener.h"
#include "Log.h"

namespace D3D11Framework
{
//------------------------------------------------------------------

	void InputMgr::Init()
	{
		_MouseWheel = _curx = _cury = 0;
		Log::Get()->Debug("InputMgr init");
	}

	void InputMgr::Close()
	{
		if (!_Listener.empty())
			_Listener.clear();
		Log::Get()->Debug("InputMgr close");
	}

	void InputMgr::SetWinRect(const RECT &winrect)
	{
		_windowrect.left = winrect.left;
		_windowrect.right = winrect.right;
		_windowrect.top = winrect.top;
		_windowrect.bottom = winrect.bottom;
	}

	void InputMgr::AddListener(InputListener *Listener)
	{
		_Listener.push_back(Listener);
	}

	void InputMgr::Run(const UINT &msg, WPARAM wParam, LPARAM lParam)
	{
		if (_Listener.empty())
			return;

		eKeyCodes KeyIndex;
		wchar_t buffer[1];
		BYTE lpKeyState[256];

		_eventcursor();// ������� �������� ����
		switch(msg)
		{
		case WM_KEYDOWN:
			KeyIndex = static_cast<eKeyCodes>(wParam);
			GetKeyboardState(lpKeyState);
			ToUnicode(wParam, HIWORD(lParam)&0xFF, lpKeyState, buffer, 1, 0);
			_eventkey(KeyIndex,buffer[0],true);
			break;
		case WM_KEYUP:
			KeyIndex = static_cast<eKeyCodes>(wParam);
			GetKeyboardState(lpKeyState);
			ToUnicode(wParam, HIWORD(lParam)&0xFF, lpKeyState, buffer, 1, 0);
			_eventkey(KeyIndex,buffer[0],false);
			break;
		case WM_LBUTTONDOWN:
			_eventmouse(MOUSE_LEFT,true);
			break;
		case WM_LBUTTONUP:
			_eventmouse(MOUSE_LEFT,false);
			break;
		case WM_RBUTTONDOWN:
			_eventmouse(MOUSE_RIGHT,true);
			break;
		case WM_RBUTTONUP:
			_eventmouse(MOUSE_RIGHT,false);
			break;
		case WM_MBUTTONDOWN:
			_eventmouse(MOUSE_MIDDLE,true);
			break;
		case WM_MBUTTONUP:
			_eventmouse(MOUSE_MIDDLE,false);
			break;
		case WM_MOUSEWHEEL:
			_mousewheel( (short)GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA );
			break;
		}
	}

	void InputMgr::_eventcursor()
	{
		POINT Position;
		GetCursorPos(&Position);	// �������� ������� ������� �������

		Position.x -= _windowrect.left;
		Position.y -= _windowrect.top;

		if (_curx==Position.x && _cury==Position.y)
			return;

		_curx = Position.x;
		_cury = Position.y;

		for(auto it = _Listener.begin(); it != _Listener.end(); ++it)
		{
			if (!(*it))
				continue;
			else if ((*it)->MouseMove(MouseEvent(_curx,_cury))==true)
				return;
		}
	}

	void InputMgr::_eventmouse(const eMouseKeyCodes Code, bool press)
	{
		for(auto it = _Listener.begin(); it != _Listener.end(); ++it)
		{
			if (!(*it))
				continue;
			// ������ ������
			if (press==true)
			{
				if ((*it)->MousePressed(MouseEventClick(Code, _curx,_cury))==true)
					return;
			}
			// ������ ��������
			else
			{
				if ((*it)->MouseReleased(MouseEventClick(Code, _curx,_cury))==true)
					return;
			}
		}
	}

	void InputMgr::_mousewheel(short Value)
	{
		if (_MouseWheel==Value)
			return;

		_MouseWheel = Value;

		for(auto it = _Listener.begin(); it != _Listener.end(); ++it)
		{
			if (!(*it))
				continue;
			else if ((*it)->MouseWheel(MouseEventWheel(_MouseWheel, _curx,_cury))==true)
				return;
		}
	}

	void InputMgr::_eventkey(const eKeyCodes KeyCode, const wchar_t ch, bool press)
	{
		for(auto it = _Listener.begin(); it != _Listener.end(); ++it)
		{
			if (!(*it))
				continue;
			// ������ ������
			if (press==true)
			{
				if ((*it)->KeyPressed(KeyEvent(ch,KeyCode))==true)
					return;
			}
			// ������ ��������
			else
			{
				if ((*it)->KeyReleased(KeyEvent(ch,KeyCode))==true)
					return;
			}
		}
	}

//------------------------------------------------------------------
}