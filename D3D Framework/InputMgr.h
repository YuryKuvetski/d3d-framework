#pragma once

#include "InputCodes.h"

namespace D3D11Framework
{
//------------------------------------------------------------------

	class InputListener;

	class InputMgr
	{
	public:
		void Init();
		void Close();

		void Run(const UINT &msg, WPARAM wParam, LPARAM lParam);

		void AddListener(InputListener *Listener);

		// ���� ����
		void SetWinRect(const RECT &winrect);

	private:
		// ������� �������� ����
		void _eventcursor();
		// ������� ������ ����
		void _eventmouse(const eMouseKeyCodes KeyCode, bool press);
		// ������� �������� ��������
		void _mousewheel(short Value);
		// ��������� ������� �������
		void _eventkey(const eKeyCodes KeyCode, const wchar_t ch, bool press);

		std::list<InputListener*> _Listener;

		RECT _windowrect;
		int _curx;
		int _cury;
		int _MouseWheel;
	};

//------------------------------------------------------------------
}