#pragma once

#include "BitmapFont.h"

namespace D3D11Framework
{
	//------------------------------------------------------------------

	class Text
	{
	public:
		Text(Render *render, BitmapFont *font);

		// text - ����� ������� ����� �������
		// static - ���� true, �� ������ ����� ������ ��������
		// size - ������������ ���������� ���� ������� ����� �������. 0 �������� ��� ��� ����� ����� ������� text
		bool Init(const std::wstring &text, bool statictext = true, int charsize = 0);
		void Draw(float r, float g, float b, float x, float y);
		void Close();

		bool SetText(const std::wstring &text);

	private:
		bool InitBuffers(const std::wstring &text);
		void RenderBuffers();
		bool Updatebuffer(const std::wstring &text);

		Render *_pRender;

		BitmapFont *_pFont;
		ID3D11Buffer *_pVertexBuffer;
		ID3D11Buffer *_pIndexBuffer;
		int _numIndex;
		int _numDrawIndex;
		int _numVertex;

		bool _isStatic;
		int _size;
	};

	//------------------------------------------------------------------
}