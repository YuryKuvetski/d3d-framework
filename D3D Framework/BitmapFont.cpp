#include "stdafx.h"
#include "BitmapFont.h"
#include <fstream>
#include "macros.h"
#include "Util.h"
#include "Shader.h"
#include "Buffer.h"

using namespace D3D11Framework;

BitmapFont::BitmapFont(Render *render)
{
	_pRender = render;
	_pConstantBuffer = nullptr;
	_pPixelBuffer = nullptr;
	_pShader = nullptr;
	_widthTex = 0;
	_heightTex = 0;
}

bool BitmapFont::Init(char *fontFilename)
{
	if (!Parse(fontFilename)){ return false; }

	_pShader = new Shader(_pRender);
	if (!_pShader) { return false; }

	if (!_pShader->LoadTexture(_file.c_str())) { return false; }

	_pShader->AddInputElementDesc("POSITION", DXGI_FORMAT_R32G32B32_FLOAT);
	_pShader->AddInputElementDesc("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT);
	if (!_pShader->CreateShader(L"BitmapFont.vs", L"BitmapFont.ps")) { return false; }


	_pConstantBuffer = Buffer::CreateConstantBuffer(_pRender->_pDevice, sizeof(BitmapFontConstantBuffer), false);
	if (!_pConstantBuffer) { return false; }

	_pPixelBuffer = Buffer::CreateConstantBuffer(_pRender->_pDevice, sizeof(BitmapFontPixelBufferType), false);
	if (!_pPixelBuffer) { return false; }

	return true;
}

bool BitmapFont::Parse(char *fontFilename)
{
	ifstream fin;
	fin.open(fontFilename);
	if (fin.fail()){ return false; }

	string Line;
	string Read, Key, Value;
	size_t i;
	while (!fin.eof())
	{
		std::stringstream LineStream;
		std::getline(fin, Line);
		LineStream << Line;

		LineStream >> Read;
		if (Read == "common")
		{
			while (!LineStream.eof())
			{
				std::stringstream Converter;
				LineStream >> Read;
				i = Read.find('=');
				Key = Read.substr(0, i);
				Value = Read.substr(i + 1);

				Converter << Value;
				if (Key == "scaleW")
					Converter >> _widthTex;
				else if (Key == "scaleH")
					Converter >> _heightTex;
			}
		}
		else if (Read == "page")
		{
			while (!LineStream.eof())
			{
				std::stringstream Converter;
				LineStream >> Read;
				i = Read.find('=');
				Key = Read.substr(0, i);
				Value = Read.substr(i + 1);

				std::string str;
				Converter << Value;
				if (Key == "file")
				{
					Converter >> str;
					wchar_t *name = CharToWChar((char*)str.substr(1, Value.length() - 2).c_str());
					_file = name;
				}
			}
		}
		else if (Read == "char")
		{
			unsigned short CharID = 0;
			CharDesc chard;

			while (!LineStream.eof())
			{
				std::stringstream Converter;
				LineStream >> Read;
				i = Read.find('=');
				Key = Read.substr(0, i);
				Value = Read.substr(i + 1);

				Converter << Value;
				if (Key == "id")
					Converter >> CharID;
				else if (Key == "x")
					Converter >> chard._srcX;
				else if (Key == "y")
					Converter >> chard._srcY;
				else if (Key == "width")
					Converter >> chard._srcW;
				else if (Key == "height")
					Converter >> chard._srcH;
				else if (Key == "xoffset")
					Converter >> chard._xOff;
				else if (Key == "yoffset")
					Converter >> chard._yOff;
				else if (Key == "xadvance")
					Converter >> chard._xAdv;
			}
			_chars.insert(std::pair<int, CharDesc>(CharID, chard));
		}
	}

	fin.close();

	return true;
}

void BitmapFont::BuildVertexArray(VertexFont *vertex, int numvert, const wchar_t *sentence)
{
	int numLetters = (int)wcslen(sentence);
	// ������ ����� ����� ���� �� ���� ������ ����� ������
	if (numLetters * 4 > numvert)
		numLetters = numvert / 4;

	float drawX = (float)_pRender->_width / 2 * (-1);
	float drawY = (float)_pRender->_height / 2;

	int index = 0;
	for (int i = 0; i<numLetters; i++)
	{
		float CharX = _chars[sentence[i]]._srcX;
		float CharY = _chars[sentence[i]]._srcY;
		float Width = _chars[sentence[i]]._srcW;
		float Height = _chars[sentence[i]]._srcH;
		float OffsetX = _chars[sentence[i]]._xOff;
		float OffsetY = _chars[sentence[i]]._yOff;

		float left = drawX + OffsetX;
		float right = left + Width;
		float top = drawY - OffsetY;
		float bottom = top - Height;
		float lefttex = CharX / _widthTex;
		float righttex = (CharX + Width) / _widthTex;
		float toptex = CharY / _heightTex;
		float bottomtex = (CharY + Height) / _heightTex;

		vertex[index]._pos = XMFLOAT3(left, top, 0.0f);
		vertex[index]._tex = XMFLOAT2(lefttex, toptex);
		index++;
		vertex[index]._pos = XMFLOAT3(right, bottom, 0.0f);
		vertex[index]._tex = XMFLOAT2(righttex, bottomtex);
		index++;
		vertex[index]._pos = XMFLOAT3(left, bottom, 0.0f);
		vertex[index]._tex = XMFLOAT2(lefttex, bottomtex);
		index++;
		vertex[index]._pos = XMFLOAT3(right, top, 0.0f);
		vertex[index]._tex = XMFLOAT2(righttex, toptex);
		index++;

		drawX += _chars[sentence[i]]._xAdv;
	}
}

void BitmapFont::Draw(unsigned int index, float r, float g, float b, float x, float y)
{
	SetShaderParameters(r, g, b, x, y);
	_pShader->Draw();
	_pRender->_pImmediateContext->DrawIndexed(index, 0, 0);
}

void BitmapFont::SetShaderParameters(float r, float g, float b, float x, float y)
{
	XMMATRIX objmatrix = XMMatrixTranslation(x, -y, 0);
	XMMATRIX wvp = objmatrix*_pRender->_ortho;
	BitmapFontConstantBuffer cb;
	cb.WVP = XMMatrixTranspose(wvp);
	_pRender->_pImmediateContext->UpdateSubresource(_pConstantBuffer, 0, NULL, &cb, 0, 0);

	_pRender->_pImmediateContext->VSSetConstantBuffers(0, 1, &_pConstantBuffer);

	BitmapFontPixelBufferType pb;
	pb._pixelColor = XMFLOAT4(r, g, b, 1.0f);
	_pRender->_pImmediateContext->UpdateSubresource(_pPixelBuffer, 0, NULL, &pb, 0, 0);

	_pRender->_pImmediateContext->PSSetConstantBuffers(0, 1, &_pPixelBuffer);
}

void BitmapFont::Close()
{
	_RELEASE(_pConstantBuffer);
	_RELEASE(_pPixelBuffer);
	_CLOSE(_pShader);
}