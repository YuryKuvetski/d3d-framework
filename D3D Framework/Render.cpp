#include "stdafx.h"
#include "Render.h"
#include "macros.h"
#include "OBJ.h"
#include "Image.h"
#include "BitmapFont.h"
#include "Text.h"
#include "Log.h"

namespace D3D11Framework
{
//------------------------------------------------------------------

	Render::Render() :
		_pImmediateContext(nullptr),
		_pDevice(nullptr),
		_pRenderTargetView(nullptr),
		_pSwapChain(nullptr),
		_pDepthStencil(nullptr),
		_pDepthStencilView(nullptr),
		_pDepthDisabledStencilState(nullptr),
		_pDepthStencilState(nullptr),
		_pAlphaEnableBlendingState(nullptr),
		_pAlphaDisableBlendingState(nullptr),
		_driverType(D3D_DRIVER_TYPE_NULL),
		_featureLevel(D3D_FEATURE_LEVEL_11_0)
	{}

	Render::~Render()
	{
	}

	void Render::Resize()
	{
		RECT rc;
		GetClientRect(_hwnd, &rc);
		_width = rc.right - rc.left;
		_height = rc.bottom - rc.top;
	}

	bool Render::CreateDevice(HWND hwnd)
	{
		_hwnd = hwnd;
		
		Resize();

		if (!CreateDevice())
		{
			Log::Get()->Err("�� ������� ������� DirectX Device");
			return false;
		}

		if (!CreateDepthStencil())
		{
			Log::Get()->Err("�� ������� ������� ����� �������");
			return false;
		}

		if (!CreateBlendingState())
		{
			Log::Get()->Err("�� ������� ������� blending state");
			return false;
		}

		_pImmediateContext->OMSetRenderTargets(1, &_pRenderTargetView, _pDepthStencilView);

		D3D11_VIEWPORT vp;
		vp.Width = _width;
		vp.Height = _height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = vp.TopLeftY = 0;
		_pImmediateContext->RSSetViewports(1, &vp);

		InitMatrix();

		return Init();
	}

	bool Render::CreateDevice()
	{
		UINT createDeviceFlags = 0;
#ifdef _DEBUG
		createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

		D3D_DRIVER_TYPE driverTypes[] =
		{
			D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_WARP,
			D3D_DRIVER_TYPE_REFERENCE,
		};
		UINT numDriverTypes = ARRAYSIZE(driverTypes);

		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};
		UINT numFeatureLevels = ARRAYSIZE(featureLevels);

		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = 1;
		sd.BufferDesc.Width = _width;
		sd.BufferDesc.Height = _height;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = _hwnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = TRUE;

		HRESULT hr = S_OK;
		for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
		{
			_driverType = driverTypes[driverTypeIndex];
			hr = D3D11CreateDeviceAndSwapChain(NULL, _driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
				D3D11_SDK_VERSION, &sd, &_pSwapChain, &_pDevice, &_featureLevel, &_pImmediateContext);
			if (SUCCEEDED(hr)) { break; }
		}
		if (FAILED(hr)) { return false; }

		ID3D11Texture2D* pBackBuffer = nullptr;
		hr = _pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
		if (FAILED(hr)){ return false; }
		
		hr = _pDevice->CreateRenderTargetView(pBackBuffer, NULL, &_pRenderTargetView);
		_RELEASE(pBackBuffer);
		if (FAILED(hr)){ return false; }

		return true;
	}

	bool Render::CreateDepthStencil()
	{
		D3D11_TEXTURE2D_DESC descDepth;
		ZeroMemory(&descDepth, sizeof(descDepth));
		descDepth.Width = _width;
		descDepth.Height = _height;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		HRESULT hr = _pDevice->CreateTexture2D(&descDepth, NULL, &_pDepthStencil);
		if (FAILED(hr)){ return false; }

		D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
		ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
		depthStencilDesc.DepthEnable = true;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
		depthStencilDesc.StencilEnable = true;
		depthStencilDesc.StencilReadMask = 0xFF;
		depthStencilDesc.StencilWriteMask = 0xFF;
		depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		hr = _pDevice->CreateDepthStencilState(&depthStencilDesc, &_pDepthStencilState);
		if (FAILED(hr)){ return false; }

		depthStencilDesc.DepthEnable = false;
		hr = _pDevice->CreateDepthStencilState(&depthStencilDesc, &_pDepthDisabledStencilState);
		if (FAILED(hr)){ return false; }

		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		ZeroMemory(&descDSV, sizeof(descDSV));
		descDSV.Format = descDepth.Format;
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;
		hr = _pDevice->CreateDepthStencilView(_pDepthStencil, &descDSV, &_pDepthStencilView);
		if (FAILED(hr)){ return false; }

		return true;
	}

	bool Render::CreateBlendingState()
	{
		D3D11_BLEND_DESC blendStateDescription;
		ZeroMemory(&blendStateDescription, sizeof(D3D11_BLEND_DESC));
		blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
		blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_DEST_ALPHA;
		blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;
		HRESULT hr = _pDevice->CreateBlendState(&blendStateDescription, &_pAlphaEnableBlendingState);
		if (FAILED(hr)){ return false; }

		blendStateDescription.RenderTarget[0].BlendEnable = FALSE;
		hr = _pDevice->CreateBlendState(&blendStateDescription, &_pAlphaDisableBlendingState);
		if (FAILED(hr)){ return false; }

		return true;
	}

	void Render::InitMatrix()
	{
		float aspect = (float)_width / (float)_height;
		_projection = XMMatrixPerspectiveFovLH(0.4f*3.14f, aspect, 1.0f, 1000.0f);
		_ortho = XMMatrixOrthographicLH((float)_width, (float)_height, 0.0f, 1.0f);
	}

	void Render::BeginFrame()
	{
		float clearColor[] = {0.0f, 0.125f, 0.3f, 1.0f};
		_pImmediateContext->ClearRenderTargetView(_pRenderTargetView, clearColor);
		_pImmediateContext->ClearDepthStencilView(_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	}

	void Render::EndFrame()
	{
		_pSwapChain->Present(0, 0);
	}

	void Render::Shutdown()
	{
		Close();
		
		if (_pImmediateContext){ _pImmediateContext->ClearState(); }

		_RELEASE(_pAlphaEnableBlendingState);
		_RELEASE(_pAlphaDisableBlendingState);
		_RELEASE(_pDepthStencil);
		_RELEASE(_pDepthStencilView);
		_RELEASE(_pDepthStencilState);
		_RELEASE(_pDepthDisabledStencilState);
		_RELEASE(_pRenderTargetView);
		_RELEASE(_pSwapChain);
		_RELEASE(_pImmediateContext);
		_RELEASE(_pDevice);
	}

	void Render::TurnZBufferOn()
	{
		int a = 0;
		//_pImmediateContext->OMSetDepthStencilState(_pDepthStencilState, 1);
	}

	void Render::TurnZBufferOff()
	{
		int a = 0;
		//_pImmediateContext->OMSetDepthStencilState(_pDepthDisabledStencilState, 1);
	}

	void Render::TurnOnAlphaBlending()
	{
		float blendFactor[4];
		blendFactor[0] = 0.0f;
		blendFactor[1] = 0.0f;
		blendFactor[2] = 0.0f;
		blendFactor[3] = 0.0f;
		//_pImmediateContext->OMSetBlendState(_pAlphaEnableBlendingState, blendFactor, 0xffffffff);
	}

	void Render::TurnOffAlphaBlending()
	{
		float blendFactor[4];
		blendFactor[0] = 0.0f;
		blendFactor[1] = 0.0f;
		blendFactor[2] = 0.0f;
		blendFactor[3] = 0.0f;
		//_pImmediateContext->OMSetBlendState(_pAlphaDisableBlendingState, blendFactor, 0xffffffff);
	}

//------------------------------------------------------------------
}