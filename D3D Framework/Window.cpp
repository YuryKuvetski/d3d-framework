#include "stdafx.h"
#include "Window.h"
#include "InputMgr.h"
#include "Log.h"

namespace D3D11Framework
{
//------------------------------------------------------------------
	
	Window *Window::_wndthis = nullptr;

	Window::Window(void) :
		_inputmgr(nullptr),
		_hwnd(0),
		_isexit(false),
		_active(true),
		_minimized(false),
		_maximized(false),
		_isresize(false)
	{
		if (!_wndthis)
			_wndthis = this;
		else
			Log::Get()->Err("Window ��� ��� ������");
	}

	bool Window::Create(const DescWindow &desc)
	{
		Log::Get()->Debug("Window Create");
		_desc = desc;

		WNDCLASSEXW wnd;

		wnd.cbSize = sizeof(WNDCLASSEXW);
		wnd.style = CS_HREDRAW | CS_VREDRAW;
		wnd.lpfnWndProc = StaticWndProc;
		wnd.cbClsExtra = 0;
		wnd.cbWndExtra = 0;
		wnd.hInstance = 0;
		wnd.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		wnd.hIconSm = wnd.hIcon;
		wnd.hCursor = LoadCursor(0, IDC_ARROW);
		wnd.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wnd.lpszMenuName = NULL;
		wnd.lpszClassName = L"D3D11F";
		wnd.cbSize = sizeof(WNDCLASSEX);

		if( !RegisterClassEx( &wnd ) )
		{
			Log::Get()->Err("�� ������� ���������������� ����");
			return false;
		}

		RECT rect = {0, 0, _desc.width, _desc.height};
		AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW | WS_VISIBLE, FALSE);

		long lwidth = rect.right - rect.left;
		long lheight = rect.bottom - rect.top;

		long lleft = (long)_desc.posx;	
		long ltop = (long)_desc.posy;

		_hwnd = CreateWindowEx(NULL, L"D3D11F", _desc.caption.c_str(), WS_OVERLAPPEDWINDOW | WS_VISIBLE,  lleft, ltop, lwidth, lheight, NULL, NULL, NULL, NULL);

		if( !_hwnd )
		{
			Log::Get()->Err("�� ������� ������� ����");
			return false;
		}

		ShowWindow(_hwnd, SW_SHOW);
		UpdateWindow(_hwnd);

		return true;
	}

	void Window::RunEvent()
	{
		MSG msg;			// ������� ����	
		// ������������� ��� ����������� �������
		while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	void Window::Close()
	{
		if (_hwnd)
			DestroyWindow(_hwnd);
		_hwnd = nullptr;

		Log::Get()->Debug("Window Close");
	}

	LRESULT Window::WndProc(HWND hwnd, UINT nMsg, WPARAM wParam, LPARAM lParam)
	{
		switch(nMsg)
		{
		case WM_CREATE:
			return 0;
		case WM_CLOSE:
			_isexit = true;
			return 0;
		case WM_ACTIVATE:
			if (LOWORD(wParam) != WA_INACTIVE)
				_active = true;
			else
				_active = false;
			return 0;
		case WM_MOVE:
			_desc.posx = LOWORD(lParam);
			_desc.posy = HIWORD(lParam);
			_UpdateWindowState();
			return 0;
		case WM_SIZE:
			if (!_desc.resizing)
				return 0;
			_desc.width = LOWORD(lParam);
			_desc.height = HIWORD(lParam);
			_isresize = true;
			if( wParam == SIZE_MINIMIZED )
			{
				_active = false;
				_minimized = true;
				_maximized = false;
			}
			else if( wParam == SIZE_MAXIMIZED )
			{
				_active = true;
				_minimized = false;
				_maximized = true;
			}
			else if( wParam == SIZE_RESTORED )
			{
				if( _minimized )
				{
					_active = true;
					_minimized = false;
				}
				else if( _maximized )
				{
					_active = true;
					_maximized = false;
				}
			}
			_UpdateWindowState();
			return 0;
		case WM_MOUSEMOVE: case WM_LBUTTONUP: case WM_LBUTTONDOWN: case WM_MBUTTONUP: case WM_MBUTTONDOWN: case WM_RBUTTONUP: case WM_RBUTTONDOWN: case WM_MOUSEWHEEL: case WM_KEYDOWN: case WM_KEYUP:
			if (_inputmgr)
				_inputmgr->Run(nMsg,wParam, lParam);
			return 0;
		}

		return DefWindowProcW( hwnd, nMsg, wParam, lParam);
	}

	void Window::SetInputMgr(InputMgr *inputmgr) 
	{
		_inputmgr = inputmgr;
		_UpdateWindowState();	
	}

	void Window::_UpdateWindowState()
	{
		RECT ClientRect;
		ClientRect.left = _desc.posx;
		ClientRect.top = _desc.posy;
		ClientRect.right = _desc.width;
		ClientRect.bottom = _desc.height;
		if (_inputmgr)
			_inputmgr->SetWinRect(ClientRect);
	}

	LRESULT CALLBACK D3D11Framework::StaticWndProc(HWND hwnd, UINT nMsg, WPARAM wParam, LPARAM lParam)
	{
		return Window::Get()->WndProc( hwnd, nMsg, wParam, lParam );
	}

//------------------------------------------------------------------
}