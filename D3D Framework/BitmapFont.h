#pragma once

#include "Render.h"

using namespace std;

namespace D3D11Framework
{
	//------------------------------------------------------------------

struct VertexFont
{
	XMFLOAT3 _pos;
	XMFLOAT2 _tex;
};

class BitmapFont
{
private:
	struct CharDesc
	{
		CharDesc() : _srcX(0), _srcY(0), _srcW(0), _srcH(0), _xOff(0), _yOff(0), _xAdv(0) {}

		short _srcX;
		short _srcY;
		short _srcW;
		short _srcH;
		short _xOff;
		short _yOff;
		short _xAdv;
	};

	struct BitmapFontConstantBuffer
	{
		XMMATRIX WVP;
	};
	struct BitmapFontPixelBufferType
	{
		XMFLOAT4 _pixelColor;
	};
public:
	BitmapFont(Render *render);

	bool Init(char *fontFilename);
	void Draw(unsigned int index, float r, float g, float b, float x, float y);
	void BuildVertexArray(VertexFont *vert, int numvert, const wchar_t *sentence);
	void Close();

private:
	bool Parse(char *fontFilename);
	void SetShaderParameters(float r, float g, float b, float x, float y);

	Render* _pRender;
	ID3D11Buffer* _pConstantBuffer;
	ID3D11Buffer* _pPixelBuffer;
	Shader* _pShader;

	unsigned short _widthTex;
	unsigned short _heightTex;
	std::wstring _file;
	std::map <int, CharDesc> _chars;
};
//------------------------------------------------------------------
}