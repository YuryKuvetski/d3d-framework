#pragma once

#include "Render.h"

namespace D3D11Framework
{
	//------------------------------------------------------------------

	struct ImageVertex
	{
		XMFLOAT3 pos;
		XMFLOAT2 tex;
	};
	struct ImageConstantBuffer
	{
		XMMATRIX Ortho;
	};

	class Image
	{
	public:
		Image(Render *render);

		bool Init(const wchar_t *textureFilename, float bitmapWidth, float bitmapHeight);
		void Draw(int positionX, int positionY);
		void Close();

	private:
		bool InitBuffers();
		void RenderBuffers();
		void RenderShader();
		void SetShaderParameters(float x, float y);

		Render *_pRender;

		ID3D11Buffer *_vertexBuffer;
		ID3D11Buffer *_indexBuffer;
		ID3D11Buffer *_constantBuffer;
		Shader* _pShader;

		int _bitmapWidth, _bitmapHeight;
		int _previousPosX, _previousPosY;
	};

	//------------------------------------------------------------------
}