#pragma once
#include "stdafx.h"

inline wchar_t* CharToWChar(char *mbString)
{
	int len = 0;
	len = (int)strlen(mbString) + 1;
	wchar_t ucString[255];
	memset(ucString, 0, sizeof(ucString));
	mbstowcs(ucString, mbString, len);
	return ucString;
}