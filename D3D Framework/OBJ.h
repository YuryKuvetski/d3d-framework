#pragma once

#include "Render.h"
#include "macros.h"

namespace D3D11Framework
{
	//------------------------------------------------------------------
	struct ObjConstantBuffer
	{
		XMMATRIX WVP;
	};

	struct ObjVertex
	{
		XMFLOAT3 _position;
		XMFLOAT3 _normal;
		XMFLOAT2 _texCoord;
	};

	struct MATERIAL
	{
		XMFLOAT4 _ambientColor;
		XMFLOAT4 _diffuseColor;
		XMFLOAT4 _specularColor;
		float _specularPower;
		int _illuminationModel;

		char _ambientTextureMap[MAX_FILENAME_LEN];
		ID3D11ShaderResourceView* _pAmbientTextureRSV;
		char _diffuseTextureMap[MAX_FILENAME_LEN];
		ID3D11ShaderResourceView* _pDiffuseTextureRSV;
		char _specularColorTextureMap[MAX_FILENAME_LEN];
		char _specularHighLightComponent[MAX_FILENAME_LEN];
		char _alphaTextureMap[MAX_FILENAME_LEN];
		char _bumpMap[MAX_FILENAME_LEN];
		char _displacementMap[MAX_FILENAME_LEN];
		char _stencilDecalTexture[MAX_FILENAME_LEN];

		MATERIAL() :
			_ambientColor(1.0f, 1.0f, 1.0f, 1.0f),
			_diffuseColor(1.0f, 1.0f, 1.0f, 1.0f),
			_specularColor(0.0f, 0.0f, 0.0f, 0.0f),
			_specularPower(10.0f),
			_illuminationModel(1),
			_pAmbientTextureRSV(nullptr)
		{
			memset(_ambientTextureMap, 0, MAX_FILENAME_LEN);
			memset(_diffuseTextureMap, 0, MAX_FILENAME_LEN);
			memset(_specularColorTextureMap, 0, MAX_FILENAME_LEN);
			memset(_specularHighLightComponent, 0, MAX_FILENAME_LEN);
			memset(_alphaTextureMap, 0, MAX_FILENAME_LEN);
			memset(_bumpMap, 0, MAX_FILENAME_LEN);
			memset(_displacementMap, 0, MAX_FILENAME_LEN);
			memset(_stencilDecalTexture, 0, MAX_FILENAME_LEN);
		}
		friend ostream& operator<<(ostream& os, const MATERIAL& mtl);
	};

	struct SUBSET
	{
		int _startIndex;
		MATERIAL _material;

		SUBSET(){ _startIndex = 0; }
		SUBSET(int startIndex){ _startIndex = startIndex; }
	};

	class OBJ
	{
	public:
		OBJ(Render *render);
		~OBJ();

		bool Init(wchar_t *name);
		void Draw(CXMMATRIX viewmatrix);
		void Close();

		// ������������� �������
		void Identity();
		void Translate(float x, float y, float z);
		void Rotate(float angle, float x, float y, float z);
		void Scale(float x, float y, float z);

		void* operator new(size_t i){ return _aligned_malloc(i, 16); }
		void operator delete(void* p){ return _aligned_free(p); }
	private:
		void LoadFromObjFileToMemory();
		void CreateBobjFromMemory();
		void CreateTXTFromMemory();
		void LoadFromBobjFileToMemory();
		
		void ClearMemory();
		MATERIAL GetMaterialFromFile(string mtlName);
		bool LoadTextures();

		bool CreateBuffers();
		void RenderBuffers();
		void SetShaderParameters(CXMMATRIX viewmatrix);
		void RenderShader();

		Render *_pRender;

		ID3D11Buffer* _pIndexBuffer;
		ID3D11Buffer* _pVertexBuffer;
		ID3D11Buffer* _pConstantBuffer;
		Shader* _pShader;
		XMMATRIX _objMatrix;

		// ����� ������
		vector<XMFLOAT3> _vertices;
		vector<XMFLOAT3> _normals;
		vector<XMFLOAT2> _texCoords;

		// ������� �������
		vector<DWORD> _indices;
		vector<ObjVertex> _clearData;
		vector<SUBSET> _subsets;
		int _numIndex;
		int _numVertex;

		wstring _objFileName;
		wstring _bobjFileName;
		wstring _mtlFileName;
		wstring _txtFileName;
	};

	//------------------------------------------------------------------
}