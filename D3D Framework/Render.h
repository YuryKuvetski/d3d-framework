#pragma once

namespace D3D11Framework
{
//------------------------------------------------------------------

	class Render
	{
		friend class OBJ;
		friend class Image;
		friend class BitmapFont;
		friend class Text;
		friend class Shader;
	public:
		Render();
		virtual ~Render();

		bool CreateDevice(HWND hwnd);
		void BeginFrame();
		void EndFrame();
		void Shutdown();

		virtual bool Init() = 0;
		virtual bool Draw() = 0;
		virtual void Close() = 0;

		void TurnZBufferOn();
		void TurnZBufferOff();

		void TurnOnAlphaBlending();
		void TurnOffAlphaBlending();

		void* operator new(size_t i){ return _aligned_malloc(i, 16); }
		void operator delete(void* p){ return _aligned_free(p); }

		HRESULT _compileShaderFromFile(WCHAR* FileName, LPCSTR EntryPoint, LPCSTR ShaderModel, ID3DBlob** ppBlobOut);
	protected:
		bool CreateDevice();
		bool CreateDepthStencil();
		bool CreateBlendingState();
		void InitMatrix();
		void Resize();

		ID3D11Device *_pDevice;
		D3D_DRIVER_TYPE _driverType;
		D3D_FEATURE_LEVEL _featureLevel;
		ID3D11DeviceContext *_pImmediateContext;
		IDXGISwapChain *_pSwapChain;
		ID3D11RenderTargetView *_pRenderTargetView;

		ID3D11Texture2D *_pDepthStencil;
		ID3D11DepthStencilView *_pDepthStencilView;

		ID3D11DepthStencilState *_pDepthStencilState;
		ID3D11DepthStencilState *_pDepthDisabledStencilState;

		ID3D11BlendState *_pAlphaEnableBlendingState;
		ID3D11BlendState *_pAlphaDisableBlendingState;

		XMMATRIX _ortho;
		XMMATRIX _projection;

		HWND _hwnd;
		unsigned int _width;
		unsigned int _height;
	};

//------------------------------------------------------------------
}