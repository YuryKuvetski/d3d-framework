#pragma once

namespace D3D11Framework
{
//------------------------------------------------------------------

	class Log
	{
	public:
		Log();
		~Log();

		static Log* Get(){return _instance;}

		void Print(const char *message, ...);
		void Debug(const char *message, ...);
		void Err(const char *message, ...);

	private:
		static Log *_instance;

		void _init();
		void _close();
		void _print(const char *levtext, const char *text);

		FILE *_file;	
	};

//------------------------------------------------------------------
}