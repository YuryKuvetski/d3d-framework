#pragma once

#include "Render.h"

namespace D3D11Framework
{
	//------------------------------------------------------------------

	class Shader
	{
	public:
		Shader(Render *render);
		// ������ ������� �� ������ �������� �� ������ CreateShader
		void AddInputElementDesc(const char *SemanticName, DXGI_FORMAT format);

		bool CreateShader(wchar_t *namevs, wchar_t *nameps);
		bool LoadTexture(const wchar_t *name);
		bool CreateSamplerState();

		void Draw();
		bool Draw(ID3D11ShaderResourceView*& newTexture);
		void Close();

	private:
		HRESULT _compileShaderFromFile(WCHAR* FileName, LPCSTR EntryPoint, LPCSTR ShaderModel, ID3DBlob** ppBlobOut);

		Render *_pRender;

		ID3D11VertexShader *_pVertexShader;
		ID3D11PixelShader *_pPixelShader;
		ID3D11InputLayout *_pLayout;
		ID3D11ShaderResourceView *_pTexture;
		ID3D11SamplerState *_pSampleState;

		D3D11_INPUT_ELEMENT_DESC *_pLayoutFormat;
		unsigned int _numLayout;
	};

	//------------------------------------------------------------------
}