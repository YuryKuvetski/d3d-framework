#pragma once
#include <clocale>
#include <ctime>

#include <sstream>
#include <string>
#include <list>
#include <vector>
#include <map>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <d3d11_1.h>
#include <DirectXMath.h>
#include <d3dcompiler.h>

using namespace std;
using namespace DirectX;


#pragma comment(lib, "d3d11.lib")
