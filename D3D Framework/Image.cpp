#include "stdafx.h"
#include "Image.h"
#include "Shader.h"
#include "macros.h"
#include "Log.h"
#include "Buffer.h"

using namespace D3D11Framework;

Image::Image(Render *render)
{
	_pRender = render;
	_vertexBuffer = nullptr;
	_indexBuffer = nullptr;
	_constantBuffer = nullptr;
	_pShader = nullptr;
}

bool Image::Init(const wchar_t *textureFilename, float bitmapWidth, float bitmapHeight)
{
	_bitmapWidth = bitmapWidth;
	_bitmapHeight = bitmapHeight;
	_previousPosX = -1;
	_previousPosY = -1;

	if (!InitBuffers()) { return false; }

	_pShader = new Shader(_pRender);
	if (!_pShader) { return false; }

	if (!_pShader->LoadTexture(textureFilename)){ return false; }
	_pShader->AddInputElementDesc("POSITION", DXGI_FORMAT_R32G32B32_FLOAT);
	_pShader->AddInputElementDesc("TEXCOORD", DXGI_FORMAT_R32G32_FLOAT);
	if (!_pShader->CreateShader(L"image.vs", L"image.ps")) { return false; }

	return true;
}

bool Image::InitBuffers()
{
	ImageVertex vertices[4];

	signed int centreW = _pRender->_width / 2 * -1;
	signed int centreH = _pRender->_height / 2;
	float left = (float)centreW;
	float right = left + _bitmapWidth;
	float top = (float)centreH;
	float bottom = top - _bitmapHeight;

	vertices[0].pos = XMFLOAT3(left, top, 0.0f);
	vertices[0].tex = XMFLOAT2(0.0f, 0.0f);

	vertices[1].pos = XMFLOAT3(right, bottom, 0.0f);
	vertices[1].tex = XMFLOAT2(1.0f, 1.0f);

	vertices[2].pos = XMFLOAT3(left, bottom, 0.0f);
	vertices[2].tex = XMFLOAT2(0.0f, 1.0f);

	vertices[3].pos = XMFLOAT3(right, top, 0.0f);
	vertices[3].tex = XMFLOAT2(1.0f, 0.0f);

	unsigned long indices[6] =
	{
		0, 1, 2,
		0, 3, 1
	};

	_vertexBuffer = Buffer::CreateVertexBuffer(_pRender->_pDevice, sizeof(ImageVertex)* 4, false, &vertices);
	if (!_vertexBuffer) { return false; }

	_indexBuffer = Buffer::CreateIndexBuffer(_pRender->_pDevice, sizeof(unsigned long)* 6, false, &indices);
	if (!_indexBuffer) { return false; }

	_constantBuffer = Buffer::CreateConstantBuffer(_pRender->_pDevice, sizeof(ImageConstantBuffer), false);
	if (!_constantBuffer) { return false; }

	return true;
}

void Image::Draw(int positionX, int positionY)
{
	RenderBuffers();
	SetShaderParameters(positionX, positionY);
	RenderShader();
}

void Image::RenderBuffers()
{
	unsigned int stride = sizeof(ImageVertex);
	unsigned int offset = 0;
	_pRender->_pImmediateContext->IASetVertexBuffers(0, 1, &_vertexBuffer, &stride, &offset);
	_pRender->_pImmediateContext->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	_pRender->_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void Image::SetShaderParameters(float x, float y)
{
	XMMATRIX objmatrix = XMMatrixTranslation(x, -y, 0.0f);
	XMMATRIX wvp = objmatrix*_pRender->_ortho;
	ImageConstantBuffer cb;
	cb.Ortho = XMMatrixTranspose(wvp);
	_pRender->_pImmediateContext->UpdateSubresource(_constantBuffer, 0, NULL, &cb, 0, 0);

	_pRender->_pImmediateContext->VSSetConstantBuffers(0, 1, &_constantBuffer);
}

void Image::RenderShader()
{
	_pShader->Draw();
	_pRender->_pImmediateContext->DrawIndexed(6, 0, 0);
}

void Image::Close()
{
	_RELEASE(_vertexBuffer);
	_RELEASE(_indexBuffer);
	_RELEASE(_constantBuffer);
	_CLOSE(_pShader);
}