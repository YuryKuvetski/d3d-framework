#include "stdafx.h"
#include "Shader.h"
#include "macros.h"
#include "Log.h"
#include "DDSTextureLoader.h"


using namespace D3D11Framework;

#define MAXLAYOUT 8

Shader::Shader(Render *render)
{
	_pRender = render;
	_pVertexShader = nullptr;
	_pPixelShader = nullptr;
	_pLayout = nullptr;
	_pSampleState = nullptr;
	_pTexture = nullptr;
	_pLayoutFormat = nullptr;
	_numLayout = 0;
}

void Shader::AddInputElementDesc(const char *SemanticName, DXGI_FORMAT format)
{
	if (!_numLayout)
	{
		_pLayoutFormat = new D3D11_INPUT_ELEMENT_DESC[MAXLAYOUT];
		if (!_pLayoutFormat) { return; }
	}
	else if (_numLayout >= MAXLAYOUT){ return; }

	D3D11_INPUT_ELEMENT_DESC &Layout = _pLayoutFormat[_numLayout];

	Layout.SemanticName = SemanticName;
	Layout.SemanticIndex = 0;
	Layout.Format = format;
	Layout.InputSlot = 0;
	if (!_numLayout)
		Layout.AlignedByteOffset = 0;
	else
		Layout.AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;

	Layout.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	Layout.InstanceDataStepRate = 0;

	_numLayout++;
}

bool Shader::CreateShader(wchar_t *namevs, wchar_t *nameps)
{
	HRESULT hr = S_OK;
	ID3DBlob *vertexShaderBuffer = nullptr;
	hr = _compileShaderFromFile(namevs, "VS", "vs_4_1", &vertexShaderBuffer);
	if (FAILED(hr))
	{
		Log::Get()->Err("�� ������� ��������� ��������� ������ %ls", namevs);
		return false;
	}

	ID3DBlob *pixelShaderBuffer = nullptr;
	hr = _compileShaderFromFile(nameps, "PS", "ps_4_1", &pixelShaderBuffer);
	if (FAILED(hr))
	{
		Log::Get()->Err("�� ������� ��������� ���������� ������ %ls", nameps);
		return false;
	}

	hr = _pRender->_pDevice->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &_pVertexShader);
	if (FAILED(hr))
	{
		Log::Get()->Err("�� ������� ������� ��������� ������");
		return false;
	}

	hr = _pRender->_pDevice->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), NULL, &_pPixelShader);
	if (FAILED(hr))
	{
		Log::Get()->Err("�� ������� ������� ���������� ������");
		return false;
	}

	hr = _pRender->_pDevice->CreateInputLayout(_pLayoutFormat, _numLayout, vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), &_pLayout);
	if (FAILED(hr))
	{
		Log::Get()->Err("�� ������� ������� ������ �����");
		return false;
	}
	_DELETE_ARRAY(_pLayoutFormat);

	_RELEASE(vertexShaderBuffer);
	_RELEASE(pixelShaderBuffer);

	return true;
}

HRESULT Shader::_compileShaderFromFile(WCHAR* FileName, LPCSTR EntryPoint, LPCSTR ShaderModel, ID3DBlob** ppBlobOut)
{
	HRESULT hr = S_OK;

	DWORD ShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	ShaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob *pErrorBlob = nullptr;
	hr = D3DCompileFromFile(FileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, EntryPoint, ShaderModel, ShaderFlags, 0, ppBlobOut, &pErrorBlob);
	if (FAILED(hr) && pErrorBlob)
	{
		Log::Get()->Err((char*)pErrorBlob->GetBufferPointer());
	}
	_RELEASE(pErrorBlob);
	return hr;
}

bool Shader::LoadTexture(const wchar_t *name)
{
	HRESULT hr = CreateDDSTextureFromFile(_pRender->_pDevice, name, NULL, &_pTexture);
	if (FAILED(hr))
	{
		Log::Get()->Err("�� ������� ��������� �������� %ls", name);
		return false;
	}
	return true;
}

bool Shader::CreateSamplerState()
{
	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	HRESULT hr = _pRender->_pDevice->CreateSamplerState(&samplerDesc, &_pSampleState);
	if (FAILED(hr))
	{
		Log::Get()->Err("�� ������� ������� sample state");
		return false;
	}
}

bool Shader::Draw(ID3D11ShaderResourceView*& newTexture)
{
	_pRender->_pImmediateContext->IASetInputLayout(_pLayout);
	_pRender->_pImmediateContext->VSSetShader(_pVertexShader, NULL, 0);
	_pRender->_pImmediateContext->PSSetShader(_pPixelShader, NULL, 0);

	if (newTexture)
		_pRender->_pImmediateContext->PSSetShaderResources(0, 1, &newTexture);

	if (_pSampleState)
		_pRender->_pImmediateContext->PSSetSamplers(0, 1, &_pSampleState);
	return true;
}

void Shader::Draw()
{
	_pRender->_pImmediateContext->IASetInputLayout(_pLayout);
	_pRender->_pImmediateContext->VSSetShader(_pVertexShader, NULL, 0);
	_pRender->_pImmediateContext->PSSetShader(_pPixelShader, NULL, 0);
	if (_pTexture)
		_pRender->_pImmediateContext->PSSetShaderResources(0, 1, &_pTexture);

	if (_pSampleState)
		_pRender->_pImmediateContext->PSSetSamplers(0, 1, &_pSampleState);
}

void Shader::Close()
{
	_RELEASE(_pVertexShader);
	_RELEASE(_pPixelShader);
	_RELEASE(_pLayout);
	_RELEASE(_pSampleState);
	_RELEASE(_pTexture);
}