#pragma once

#include "Window.h"
#include "Render.h"
#include "InputMgr.h"
#include "Log.h"

namespace D3D11Framework
{
//------------------------------------------------------------------
	struct FrameworkDesc
	{
		DescWindow _wnd;
		Render *_pRender;
	};

	class Framework
	{
	public:
		Framework();
		~Framework();

		bool Init(const FrameworkDesc &descFramework);
		void Run();
		void Close();

		void AddInputListener(InputListener *listener);
	protected:	
		bool _frame();	

		FrameworkDesc _descFramework;
		Window *_pWnd;
		Render *_pRender;
		InputMgr *_pInput;
		Log _log;
		bool _init;		// ���� ���� ����������������
	};

//------------------------------------------------------------------
}