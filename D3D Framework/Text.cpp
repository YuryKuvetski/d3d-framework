#include "stdafx.h"
#include "Text.h"
#include "macros.h"
#include "Buffer.h"

using namespace D3D11Framework;

Text::Text(Render *render, BitmapFont *font)
{
	_pRender = render;
	_pFont = font;
	_pVertexBuffer = nullptr;
	_pIndexBuffer = nullptr;
	_numIndex = 0;
	_numDrawIndex = 0;
	_numVertex = 0;
	_isStatic = true;
	_size = 0;
}

bool Text::Init(const std::wstring &text, bool statictext, int charsize)
{
	_isStatic = statictext;
	_size = charsize;
	if (!InitBuffers(text)) { return false; }

	return true;
}

bool Text::InitBuffers(const std::wstring &text)
{
	if (!_size || (_size<text.size()))
		_size = text.size();

	_numVertex = _size * 4;
	_numIndex = _size * 6;
	_numDrawIndex = text.size() * 6;
	if (_numDrawIndex > _numIndex)
		_numDrawIndex = _numIndex;

	VertexFont *vertex = new VertexFont[_numVertex];
	if (!vertex) { return false; }

	unsigned long *indices = new unsigned long[_numIndex];
	if (!indices) { return false; }

	_pFont->BuildVertexArray(vertex, _numVertex, text.c_str());

	for (int i = 0; i<_numIndex / 6; i++)
	{
		indices[i * 6 + 0] = i * 4 + 0;
		indices[i * 6 + 1] = i * 4 + 1;
		indices[i * 6 + 2] = i * 4 + 2;
		indices[i * 6 + 3] = i * 4 + 0;
		indices[i * 6 + 4] = i * 4 + 3;
		indices[i * 6 + 5] = i * 4 + 1;
	}

	_pVertexBuffer = Buffer::CreateVertexBuffer(_pRender->_pDevice, sizeof(VertexFont)*_numVertex, !_isStatic, vertex);
	if (!_pVertexBuffer) { return false; }

	_pIndexBuffer = Buffer::CreateIndexBuffer(_pRender->_pDevice, sizeof(unsigned long)*_numIndex, false, indices);
	if (!_pIndexBuffer) { return false; }

	_DELETE_ARRAY(vertex);
	_DELETE_ARRAY(indices);
	return true;
}

void Text::Draw(float r, float g, float b, float x, float y)
{
	RenderBuffers();
	_pFont->Draw(_numDrawIndex, r, g, b, x, y);
}

void Text::RenderBuffers()
{
	unsigned int stride = sizeof(VertexFont);
	unsigned int offset = 0;
	_pRender->_pImmediateContext->IASetVertexBuffers(0, 1, &_pVertexBuffer, &stride, &offset);
	_pRender->_pImmediateContext->IASetIndexBuffer(_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	_pRender->_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

bool Text::SetText(const std::wstring &text)
{
	// ����������� ����� ������ ��������
	if (_isStatic)
		return false;
	// ���� ����� ������ ��� ����� ��������, �� �� ������ �������� ��������
	_numDrawIndex = text.size() * 6;
	return Updatebuffer(text);
}

bool Text::Updatebuffer(const std::wstring &text)
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	HRESULT result = _pRender->_pImmediateContext->Map(_pVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(result)) { return false; }

	VertexFont *verticesPtr = (VertexFont*)mappedResource.pData;

	_pFont->BuildVertexArray(verticesPtr, _numVertex, text.c_str());

	_pRender->_pImmediateContext->Unmap(_pVertexBuffer, 0);

	return true;
}

void Text::Close()
{
	_RELEASE(_pVertexBuffer);
	_RELEASE(_pIndexBuffer);
}

