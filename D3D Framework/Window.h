#pragma once

namespace D3D11Framework
{
//------------------------------------------------------------------
	
	class InputMgr;

	struct DescWindow
	{
		DescWindow() : 
			caption(L""),
			width(640),
			height(480),
			posx(200),
			posy(20),
			resizing(true)
		{}

		int posx;
		int posy;
		std::wstring caption;	///< ��������� ����
		int width;				///< ������ ���������� ����� ����
		int height;				///< ������ ���������� ����� ����
		bool resizing;
	};

	class Window
	{
	public:
		Window();

		static Window* Get(){return _wndthis;}

		// ������� ����
		bool Create(const DescWindow &desc);

		// ��������� ������� ����
		void RunEvent();

		// ������� ����.
		void Close();

		void SetInputMgr(InputMgr *inputmgr);
		
		HWND GetHWND() const {return _hwnd;}
		int Window::GetLeft() const {return _desc.posx;}
		int Window::GetTop() const {return _desc.posy;}
		int Window::GetWidth() const {return _desc.width;}
		int Window::GetHeight() const {return _desc.height;}
		// ������� ��������� ����
		const std::wstring& GetCaption() const {return _desc.caption;}

		// ��������, ���� �� ��������� � ������
		bool IsExit() const {return _isexit;}
		// �������� �� ���������� ����
		bool IsActive() const {return _active;}
		// �������� �� ��������� ����
		// ��������������: ����� ������ ��������� ���� �� ��������� �������
		bool IsResize() 
		{
			bool ret = _isresize;
			_isresize = false;
			return ret;
		}

		// ��������� �������
		LRESULT WndProc(HWND hwnd, UINT nMsg, WPARAM wParam, LPARAM lParam);
	private:
		void _UpdateWindowState();

		static Window *_wndthis;

		DescWindow _desc;	// �������� ����
		InputMgr *_inputmgr;
		HWND _hwnd;		// ���������� ����	
		bool _isexit;		// ���� ���������� � ������� ������	
		bool _active;		// ���� �������?
		bool _minimized;
		bool _maximized;
		bool _isresize;	// ���� ���� �������� ������
	};

	// ��������� �������
	static LRESULT CALLBACK StaticWndProc(HWND hwnd, UINT nMsg, WPARAM wParam, LPARAM lParam);

//------------------------------------------------------------------
}